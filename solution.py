example = """
print("Here is your solution!")
print(42)
"""

titanic_smith = """titanic[(titanic["Name"].str.contains("Smith"))&(titanic["Sex"]=="female")]"""
titanic_name_and_fare_firstclass = """titanic[titanic["Pclass"]==1][["Name","Fare"]]"""
titanic_most_expensive_tickets = """titanic[['Name','Fare']].sort_values(by=['Fare'], ascending=False).head(1)"""
titanic_survival_rate = """titanic['Survived'].mean()"""
titanic_survival_rate_by_sex = """
titanic.groupby('Sex').mean()['Survived']
"""
titanic_survival_rate_by_sex_plot = """
surv = titanic.groupby(['Sex'])[['Survived']].mean()*100
fig, axis1 = plt.subplots(1,1,figsize=(16,6))
ax = surv.plot.bar(ax=axis1, color='blue', title='Survival Rate by Sex')
ax.set_ylabel('Survival Rate')
"""
titanic_max_fee_by_pclass = """titanic.groupby('Pclass').max()['Fare']"""
titanic_xlsx = """
file_name = 'titanic.xlsx'
writer = pd.ExcelWriter(file_name)

passengers.to_excel(writer,'Passengers_2')
writer.sheets['Passengers_2'].set_column(1, 1, 60) 
writer.sheets['Passengers_2'].conditional_format('E2:E5000', {'type': '2_color_scale'})
passengers.to_excel(writer,'Passengers_3')
writer.sheets['Passengers_3'].set_column(1, 1, 60) 
writer.sheets['Passengers_3'].conditional_format('E2:E5000', {'type': '3_color_scale'})
writer.save()
"""

titanic_db_fare = """
connection = sqlite3.connect('titanic.db')
sql_statement = "SELECT * from Passengers where Fare = 7.7750"
db_data = pd.read_sql(sql_statement, connection)
connection.close()
db_data
"""


risk_mean_deviation = """
risk_has_diff = risk_data_merged[risk_data_merged["diff_abs"] > 0]
risk_has_diff['diff_abs'].mean()
"""
risk_deviation_plot = """
fig, axis1 = plt.subplots(1, 1, figsize=(16,6))
ax = risk_has_diff["diff_abs"].sort_values().plot.bar(ax=axis1, color='blue', title='Risk Deviation Plot')
ax.set_ylabel('Risk Deviations')
ax.set_xlabel('Index')
"""


pred_model_logreg = """
from sklearn.linear_model import LogisticRegression
from sklearn.model_selection import train_test_split
# Partition data
titanic_data_train, titanic_data_test, titanic_target_train, titanic_target_test = train_test_split(
    titanic_data, titanic_target, test_size=0.5, random_state=42)
# Train
logistic = LogisticRegression()
logistic.fit(titanic_data_train, titanic_target_train)
# Predict
prediction_is = logistic.predict(titanic_data_train)
prediction_os = logistic.predict(titanic_data_test)
# Evaluate
print("Death rate train: ", 1 - np.mean(titanic_target_train))
print("Death rate test: ", 1 - np.mean(titanic_target_test))
print("Model accuracy in sample: ", np.mean(prediction_is == titanic_target_train))
print("Model accuracy out of sample: ", np.mean(prediction_os == titanic_target_test))
"""
pred_model_inverted = """
from sklearn.linear_model import LogisticRegression
from sklearn.model_selection import train_test_split 

titanic_subset = titanic[['Age', 'Pclass', 'Survived']].copy()
titanic_subset['is_male'] = titanic['Sex'] == 'male' 
titanic_subset.dropna(inplace =True) # deletes rows containing NaNs in this dataframe
titanic_target = titanic_subset['is_male']
titanic_data = titanic_subset[['Age', 'Pclass', 'Survived']]

# Partition data
titanic_data_train, titanic_data_test, titanic_target_train, titanic_target_test = train_test_split(
    titanic_data, titanic_target, test_size=0.5, random_state=42)
# Train
logistic = LogisticRegression()
logistic.fit(titanic_data_train, titanic_target_train)
# Predict
prediction_is = logistic.predict(titanic_data_train)
prediction_os = logistic.predict(titanic_data_test)
# Evaluate
print("Is_male train: ", 1 - np.mean(titanic_target_train))
print("Is_male test: ", 1 - np.mean(titanic_target_test))
print("Model accuracy in sample: ", np.mean(prediction_is == titanic_target_train))
print("Model accuracy out of sample: ", np.mean(prediction_os == titanic_target_test))"""

automatic_email = """
import os
import pyautogui as ag
import time
wait_time = 0.075
ag.PAUSE = wait_time

size = ag.size()
os.startfile(r"Outlook.exe")
time.sleep(15)
ag.hotkey('ctrl', 'n')
time.sleep(2)
ag.hotkey('tab')
ag.hotkey('tab')
ag.hotkey('tab')
time.sleep(1)
ag.typewrite("This is some example text.", interval=wait_time)
time.sleep(1)
"""

gui_write_in_outlook = """
import tkinter as tk
import os
import pyautogui as ag
import time

def print_in_outlook():
    wait_time = 0.075
    ag.PAUSE = wait_time
    size = ag.size()
    os.startfile(r"Outlook.exe")
    time.sleep(15)
    ag.hotkey('ctrl', 'n')
    time.sleep(2)
    ag.hotkey('tab')
    ag.hotkey('tab')
    ag.hotkey('tab')
    time.sleep(1)
    text =  input_1.get("1.0","end")
    ag.typewrite(text, interval=wait_time)
    time.sleep(1)


# define callback functions
def print_button_n_pressed(n):
    print("Button %d pressed!" % n)
    
def callback_button1():
    print_button_n_pressed(1)
    print_in_outlook()
    
def callback_button2():
    print_button_n_pressed(2)
    print(input_1.get("1.0","end"))
    
# Create main window
top = tk.Tk()

# Add two buttons
label_1 = tk.Label(top, text='My Buttons')
b1 = tk.Button(top, text='Create E-Mail', command=callback_button1)
b2 = tk.Button(top, text='Button 2', command=callback_button2)

# Add text field
label_2 = tk.Label(top, text='Please enter text for E-Mail Body')
input_1 = tk.Text(top)

# pack them to main window
label_1.pack()
b1.pack()
b2.pack()
label_2.pack()
input_1.pack()

# start the main loop
top.mainloop()
"""

data_with_gaps = """
data_with_gaps[~data_with_gaps.isnull().any(axis=1)]
#data_with_gaps[data_with_gaps.notnull().all(axis=1)]
"""

qgrid_titanic = """
qgrid_titanic = qgrid.show_grid(titanic, grid_options={'forceFitColumns': False}, show_toolbar=True)
qgrid_titanic
"""

qgrid_titanic_edit = """
new_titanic = qgrid_titanic.get_changed_df()
""" 
